# -*- coding: utf-8 -*-
import os
from lms.envs.devstack import *

####### Settings common to LMS and CMS
import json
import os

from xmodule.modulestore.modulestore_settings import update_module_store_settings

# Mongodb connection parameters: simply modify `mongodb_parameters` to affect all connections to MongoDb.
mongodb_parameters = {
    "host": "mongodb",
    "port": 27017,
    
    "user": None,
    "password": None,
    
    "db": "openedx",
}
DOC_STORE_CONFIG = mongodb_parameters
CONTENTSTORE = {
    "ENGINE": "xmodule.contentstore.mongo.MongoContentStore",
    "ADDITIONAL_OPTIONS": {},
    "DOC_STORE_CONFIG": DOC_STORE_CONFIG
}
# Load module store settings from config files
update_module_store_settings(MODULESTORE, doc_store_settings=DOC_STORE_CONFIG)
DATA_DIR = "/openedx/data/"
for store in MODULESTORE["default"]["OPTIONS"]["stores"]:
   store["OPTIONS"]["fs_root"] = DATA_DIR

# Behave like memcache when it comes to connection errors
DJANGO_REDIS_IGNORE_EXCEPTIONS = True

# Elasticsearch connection parameters
ELASTIC_SEARCH_CONFIG = [{
  
  "host": "elasticsearch",
  "port": 9200,
}]

CONTACT_MAILING_ADDRESS = "BioMedVis - http://local.overhang.io"

DEFAULT_FROM_EMAIL = ENV_TOKENS.get("DEFAULT_FROM_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
DEFAULT_FEEDBACK_EMAIL = ENV_TOKENS.get("DEFAULT_FEEDBACK_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
SERVER_EMAIL = ENV_TOKENS.get("SERVER_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
TECH_SUPPORT_EMAIL = ENV_TOKENS.get("TECH_SUPPORT_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
CONTACT_EMAIL = ENV_TOKENS.get("CONTACT_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
BUGS_EMAIL = ENV_TOKENS.get("BUGS_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
UNIVERSITY_EMAIL = ENV_TOKENS.get("UNIVERSITY_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
PRESS_EMAIL = ENV_TOKENS.get("PRESS_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
PAYMENT_SUPPORT_EMAIL = ENV_TOKENS.get("PAYMENT_SUPPORT_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
BULK_EMAIL_DEFAULT_FROM_EMAIL = ENV_TOKENS.get("BULK_EMAIL_DEFAULT_FROM_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
API_ACCESS_MANAGER_EMAIL = ENV_TOKENS.get("API_ACCESS_MANAGER_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])
API_ACCESS_FROM_EMAIL = ENV_TOKENS.get("API_ACCESS_FROM_EMAIL", ENV_TOKENS["CONTACT_EMAIL"])

# Get rid completely of coursewarehistoryextended, as we do not use the CSMH database
INSTALLED_APPS.remove("lms.djangoapps.coursewarehistoryextended")
DATABASE_ROUTERS.remove(
    "openedx.core.lib.django_courseware_routers.StudentModuleHistoryExtendedRouter"
)

# Set uploaded media file path
MEDIA_ROOT = "/openedx/media/"

# Add your MFE and third-party app domains here
CORS_ORIGIN_WHITELIST = []

# Video settings
VIDEO_IMAGE_SETTINGS["STORAGE_KWARGS"]["location"] = MEDIA_ROOT
VIDEO_TRANSCRIPTS_SETTINGS["STORAGE_KWARGS"]["location"] = MEDIA_ROOT

GRADES_DOWNLOAD = {
    "STORAGE_TYPE": "",
    "STORAGE_KWARGS": {
        "base_url": "/media/grades/",
        "location": "/openedx/media/grades",
    },
}

ORA2_FILEUPLOAD_BACKEND = "filesystem"
ORA2_FILEUPLOAD_ROOT = "/openedx/data/ora2"
ORA2_FILEUPLOAD_CACHE_NAME = "ora2-storage"

# Change syslog-based loggers which don't work inside docker containers
LOGGING["handlers"]["local"] = {
    "class": "logging.handlers.WatchedFileHandler",
    "filename": os.path.join(LOG_DIR, "all.log"),
    "formatter": "standard",
}
LOGGING["handlers"]["tracking"] = {
    "level": "DEBUG",
    "class": "logging.handlers.WatchedFileHandler",
    "filename": os.path.join(LOG_DIR, "tracking.log"),
    "formatter": "standard",
}
LOGGING["loggers"]["tracking"]["handlers"] = ["console", "local", "tracking"]
# Silence some loggers (note: we must attempt to get rid of these when upgrading from one release to the next)
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning, module="newrelic.console")
warnings.filterwarnings("ignore", category=DeprecationWarning, module="lms.djangoapps.course_wiki.plugins.markdownedx.wiki_plugin")
warnings.filterwarnings("ignore", category=DeprecationWarning, module="wiki.plugins.links.wiki_plugin")

# Email
EMAIL_USE_SSL = False
# Forward all emails from edX's Automated Communication Engine (ACE) to django.
ACE_ENABLED_CHANNELS = ["django_email"]
ACE_CHANNEL_DEFAULT_EMAIL = "django_email"
ACE_CHANNEL_TRANSACTIONAL_EMAIL = "django_email"
EMAIL_FILE_PATH = "/tmp/openedx/emails"

LOCALE_PATHS.append("/openedx/locale/contrib/locale")
LOCALE_PATHS.append("/openedx/locale/user/locale")

# Allow the platform to include itself in an iframe
X_FRAME_OPTIONS = "SAMEORIGIN"


JWT_AUTH["JWT_ISSUER"] = "http://local.overhang.io/oauth2"
JWT_AUTH["JWT_AUDIENCE"] = "openedx"
JWT_AUTH["JWT_SECRET_KEY"] = "VEqs5WgnmlX3ijM165pkOAKM"
JWT_AUTH["JWT_PRIVATE_SIGNING_JWK"] = json.dumps(
    {
        "kid": "openedx",
        "kty": "RSA",
        "e": "AQAB",
        "d": "QOdaZuzEjeCixhwBdE7SlBtwzaKUcinLgepXImW-SST-2w95xTUIQ4q92PP2MNrUBNlr_gYzrwJv5FYBUBs-4uITk_zLcxyfMIdj1VoZWP74CRkZVxUZ458imNPEEBp6xviJJfMrxNqJajAwqN1wslm3mweDWu47hIMtl-hQ6rz-u-zOvYM9PsD5_PVS-Y2NUqDjBlajSEuVxmfQgr3zqhD6ZLZx_86mw6bthzi1ywrWfPcv-skCXTvIlbOAZ0SB7uXWnUz9pzm870HhcxOuaCs_4iXV9RE1k4hp575FXzNLlI4o1wOJTtQcNSOonzTAxjZVFvpN-hvCfvcYVOUlpQ",
        "n": "zSVSQAgLJQLdxBd6ULAyT7jYeS-ztRV_W4Rf19wax0CgzW0l1QgP3C7C4wnsGFXYGq-25sWtZ29VgN23JO3n8Cis4KNc5o-OrDRtkpif5tEgarA6YHgelDu18d4eoVbgC-TD0pffKI5b-oNj7Xs7XawhEsZA76RR5EQOPJVhBrddoxJoe9R4rTI6FUATYU_0bkPSBcLQ5wkDi5h46pz5ZeeITcK7uRXFJXPmNrF-MkXsltssz3kTkPa7d3yHOBiV5IRzHMVmzxLGNWLmXRnrbnT5fgPM4-K7nVgxUG9ng8Umm8v_YP6N7uKAbb7M2f4Glqzqycd1XwdMkRB4t1fviw",
        "p": "3hPgFrDqSUxf245xtSuFw2n7rLZhIVBkyPd9IEBmQCKDAw2-s34D27g_dIaTF310GtxkbZlyh1hDvxIBuzWa8HQmG2u_QblGHaRymBGHVNEhCkDbnpEkkrBLxLdmbZG35aynOJxE3ubw0UH_D3KEmX2wY_7j7ECwL9-PkC4Gxuc",
        "q": "7HtXvwASvYlm9qMc6vAX1AhPdY3JCzHVWUpvCE55GAXhWdyiTnFM6LaBC9IbHehm51sXMPkCSuFjeFZww5ebjaeJLDLwg6UWxEvvnKQSO8uG3Q28u83EpwMpT025l7dvMg7yChxWj7vwiRwIkBmhd2H6YXPiQiB2AhJGXQhYUb0",
    }
)
JWT_AUTH["JWT_PUBLIC_SIGNING_JWK_SET"] = json.dumps(
    {
        "keys": [
            {
                "kid": "openedx",
                "kty": "RSA",
                "e": "AQAB",
                "n": "zSVSQAgLJQLdxBd6ULAyT7jYeS-ztRV_W4Rf19wax0CgzW0l1QgP3C7C4wnsGFXYGq-25sWtZ29VgN23JO3n8Cis4KNc5o-OrDRtkpif5tEgarA6YHgelDu18d4eoVbgC-TD0pffKI5b-oNj7Xs7XawhEsZA76RR5EQOPJVhBrddoxJoe9R4rTI6FUATYU_0bkPSBcLQ5wkDi5h46pz5ZeeITcK7uRXFJXPmNrF-MkXsltssz3kTkPa7d3yHOBiV5IRzHMVmzxLGNWLmXRnrbnT5fgPM4-K7nVgxUG9ng8Umm8v_YP6N7uKAbb7M2f4Glqzqycd1XwdMkRB4t1fviw",
            }
        ]
    }
)
JWT_AUTH["JWT_ISSUERS"] = [
    {
        "ISSUER": "http://local.overhang.io/oauth2",
        "AUDIENCE": "openedx",
        "SECRET_KEY": "VEqs5WgnmlX3ijM165pkOAKM"
    }
]

# Disable codejail support
# explicitely configuring python is necessary to prevent unsafe calls
import codejail.jail_code
codejail.jail_code.configure("python", "nonexistingpythonbinary", user=None)
# another configuration entry is required to override prod/dev settings
CODE_JAIL = {
    "python_bin": "nonexistingpythonbinary",
    "user": None,
}

# Custom features
# LTI 1.3 will be enabled by default after lilac, and it's going to be a big
# deal, so we enable it early. We should remove this once the feature flag is
# deprecated.
FEATURES["LTI_1P3_ENABLED"] = True

FEATURES["ENABLE_COURSE_DISCOVERY"] = False
######## End of settings common to LMS and CMS

######## Common LMS settings
LOGIN_REDIRECT_WHITELIST = ["studio.local.overhang.io"]

# Better layout of honor code/tos links during registration
REGISTRATION_EXTRA_FIELDS["terms_of_service"] = "required"
REGISTRATION_EXTRA_FIELDS["honor_code"] = "hidden"

# Fix media files paths
PROFILE_IMAGE_BACKEND["options"]["location"] = os.path.join(
    MEDIA_ROOT, "profile-images/"
)

COURSE_CATALOG_VISIBILITY_PERMISSION = "see_in_catalog"
COURSE_ABOUT_VISIBILITY_PERMISSION = "see_about_page"

# Allow insecure oauth2 for local interaction with local containers
OAUTH_ENFORCE_SECURE = False

# Email settings
class LazyStaticAbsoluteUrl:
    """
    Evaluates a static asset path lazily at runtime
    """
    def __init__(self, path):
        self.path = path

    def __str__(self):
        from django.conf import settings
        from django.contrib.staticfiles.storage import staticfiles_storage
        return settings.LMS_ROOT_URL + staticfiles_storage.url(self.path)

    def to_json(self):
        # This method is required for json serialization by edx-ace, notably for
        # serialization of the registration email. See
        # edx_ace.serialization.MessageEncoder.
        return str(self)
# We need a lazily-computed logo url to capture the url of the theme-specific logo.
DEFAULT_EMAIL_LOGO_URL = LazyStaticAbsoluteUrl("images/logo.png")

# Create folders if necessary
for folder in [DATA_DIR, LOG_DIR, MEDIA_ROOT, STATIC_ROOT_BASE, ORA2_FILEUPLOAD_ROOT]:
    if not os.path.exists(folder):
        os.makedirs(folder)



######## End of common LMS settings

# Setup correct webpack configuration file for development
WEBPACK_CONFIG_PATH = "webpack.dev.config.js"

SESSION_COOKIE_DOMAIN = ".local.overhang.io"

LMS_BASE = "local.overhang.io:8000"
LMS_ROOT_URL = "http://{}".format(LMS_BASE)
LMS_INTERNAL_ROOT_URL = LMS_ROOT_URL
SITE_NAME = LMS_BASE
CMS_BASE = "studio.local.overhang.io:8001"
CMS_ROOT_URL = "http://{}".format(CMS_BASE)
LOGIN_REDIRECT_WHITELIST.append(CMS_BASE)

FEATURES['ENABLE_COURSEWARE_MICROFRONTEND'] = False
COMMENTS_SERVICE_URL = "http://forum:4567"

LOGGING["loggers"]["oauth2_provider"] = {
    "handlers": ["console"],
    "level": "DEBUG"
}


