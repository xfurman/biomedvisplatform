◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 🧩 Repository Structure

This repository contains the necessary files for the Tutor distribution.

- `data` folder stores all platform data, including databases with created courses and registered users.
- `docs` folder stores this markdown documentation.
- `env` folder is regenerated every time the platform's configuration is saved. It is not recommended to manually modify its contents, as the changes are overwritten on `bmv_save` and `bmv_render`.
- `plugins` folder stores all YAML and Python plugins.
- `themes` folder stores custom themes. It contains one folder with the Indigo theme.
- `README.md` file is the root of this manual.
- `config.yml` file contains all platform settings.

## Theme folder

The `themes/indigo` folder contains the platform's theme and page templates, as visualised below.

```
themes/indigo
├─ theme
│  ├─ cms
│  ├─ lms
│  │  ├─ static
│  │  │  ├─ images
│  │  │  ├─ sass
│  │  ├─ templates
│  │  │  ├─ static_templates
│  ├─ snippets
│  │  │  ├─ biomedvis.html
├─ bmv-config.yml
├─ config.yml
```

- The file named `bmv-config.yml` overrides `config.yml` settings, so the platform-controlled file can be left untouched to apply changes.
    - For instance, to set the theme colour, modify the `INDIGO_PRIMARY_COLOR` setting.
    - Similarly, the platform's welcome message, links in the footer and more can be modified. 
    - For other available settings, refer to `tutor config` section in the Tutor documentation.

- The folder named `theme` has `cms` and `lms` folders containing the CMS and LMS modifications.
- Moreover, the added `snippets` folder contains BioMedVis templates, specifically `biomedvis.html`.
- The `biomedvis.html` template contains functions simplifying the works with courses and their associated 🟢 **Bio**, 🔵 **Med** and 🟠 **Vis** tags and **difficulty levels**.

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Further Development](../docs/development.md)
