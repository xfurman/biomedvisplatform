◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# ⬇️ Installation Instructions

This manual is written for BioMedVis setup on Linux or Windows 11/10 with Windows Subsystem for Linux (WSL) installed.

In case the current Tutor installation is not working, refer to the [unistallation section](https://docs.tutor.overhang.io/install.html#uninstallation) and then continue with the [second section](#2-install-tutor).

## 1 Install Dependencies

### 1.1 Windows Setup

If the host operating system is UNIX-based, continue to the [following subsection](#12-linux-setup).

Cmd refers to Windows Command Prompt in the following sections, and terminal refers to WSL Command-Line.

1. Install **Windows Subsystem for Linux**, specifically WSL 2. In case of version 1 is installed, update it to version 2.
    - Further instructions on installation and updating can be found at the [official WSL website](https://docs.microsoft.com/en-us/windows/wsl/install).
    - To check the current version of WSL components, use `wsl -l -v` in the cmd.
    - Check that WSL and the chosen Linux distribution are on version 2.
2. Install **Docker for Windows**.
    - Further instructions on installation and updating can be found at the [official Docker website](https://docs.docker.com/desktop/windows/install).
    - Both Docker Engine and Docker Compose are included in the Windows version.
    - Check that Docker is working in WSL using the testing command `docker run hello-world` in the terminal.
3. It is recommended to modify a few **Docker settings**.
    - Open Settings, go to the general section and check: 
        - *Start Docker Desktop when you log in*.
        - *Use the WSL 2 based engine*.
        - *Use Docker Compose V2*.
    - If Docker is only working in admin mode, the user group is not correctly set up for the current user.
    - To add a user to the user group, use:
        - Run `sudo groupadd docker` in the terminal.
        - Continue with `sudo usermod -aG docker $USER` in the terminal.
        - Reboot the WSL machine by shutting it down using `wsl.exe --shutdown` in cmd and reopening.
        - Test if it works by running `docker run hello-world` in the terminal and checking the output.

### 1.2 Linux Setup

When the host OS is UNIX-based, it is only necessary to install Docker.

Both Docker Engine and Docker Compose are necessary.

1. Install the **Docker Engine**.
    - Follow the [official Docker Engine instructions](https://docs.docker.com/engine/install/).
    - Test if it works by running `docker run hello-world` and checking the output.

2. Install the **Docker Compose**.
    - Follow the [official Docker Compose instructions](https://docs.docker.com/compose/install/).
    - Test if it works by running `docker-compose --version`.

## 2. Install Tutor

Before continuing, please read the official Tutor [introduction section](https://docs.tutor.overhang.io/intro.html) and [install section](https://docs.tutor.overhang.io/install.html).

1. Install dependencies by running `sudo apt install python3 python3-pip libyaml-dev`.
2. To install **Tutor**, run `pip install tutor==12.2.0`.
3. After completion, test if it works by running `tutor` or `pip list`.
4. Optionally, try to run `tutor local quickstart` to start a new platform.
    - This step is unnecessary and takes much time.
    - Proceed if you do not want to use the BioMedVis platform image.

## 4. Clone the BioMedVis Repository

1. The following steps assume that that `/bmv` folder exists.
2. To create the folder, run `mkdir /bmv`.
3. Switch to the newly created folder using `cd /bmv`.
4. Clone the repository by running `git clone https://gitlab.fi.muni.cz/xfurman/biomedvisplatform.git`.

## 5. Configure Tutor Distribution

1. Change the source for the Tutor distribution and the related plugins.
    - To permanently change the source, edit the profile file using `nano ~/.profile`.
    - Go to the end of the file and add `export TUTOR_ROOT="/bmv/biomedvisplatform/"` there.
    - Also add `export TUTOR_PLUGINS_ROOT="/bmv/biomedvisplatform/plugins/"` there.
    - Save changes and reopen bash to see changes.
    - Check that `tutor config printroot` and `tutor plugins printroot` point to the directories mentioned above.
2. Set the Tutor theme that contains modifications.
    - Run `tutor local settheme indigo`.
    - Also run `tutor dev settheme indigo`.
    - Without this step, almost any changes made as the part of the BioMedVis modifications would not be visible.
3. Set up the BioMedVis-specific commands to speed up the development.
    - Copy the contents of [`bmv-commands.txt`](./bmv-commands.txt) file.
    - To add the commands, edit the shell script using `nano ~/.bashrc`.
    - Go to the end of the `~/.bashrc` file and add the copied contents there.
    - Save changes and reopen bash to use the commands.
    - For more information, refer to the [commands manual](../docs/commands.md).
4. To propagate changes, use `bmv_save` and `bmv_render`.
5. To apply changes, rebuild the platform using `bmv_allbuild`. Before running it, assigning the correct access rights might be necessary. To do so, use `sudo chmod -R 777 /bmv`.

## 6. Run and Develop the BioMedVis Platform

1. To initialise the platform services on the current machine, run `bmv_init`.

2. To initially launch the platform, use `bmv_quickstart` and follow the instructions.

3. Refer to [platform operation manual](./platform.md) and [development manual](./development.md) for more information.

<br>

---

◀️ [Go Back](../README.md#manuals)
 | Next manual: [Commands](../docs/commands.md)
