◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 📢 Hosting Information

The platform is hosted using [Stratus.FI](https://www.fi.muni.cz/tech/unix/stratus.html.cs) cloud. The machine used for the production version of the platform runs on Debian 11. The persistent image option was chosen during the setup, as this option is recommended for production use. The production version of the BioMedVis platform was set up per the [installation section](./installation.md).

To view the production version of the platform, visit [biomedvis.fi.muni.cz](http://biomedvis.fi.muni.cz).

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Common Issues](../docs/issues.md)
