◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 🔬 Further Development

To start the platform in development mode, use the `bmv_dev` command. In case the Studio is also needed, use `bmv_devstudio` instead.

- LMS is available at [local.overhang.io:8000](http://local.overhang.io:8000).
- Studio is available at [studio.local.overhang.io:8001](http://studio.local.overhang.io:8001).

In order to stop the platform, use the `bmv_stop` command.

## Integrated Development Environment

Any modern environment is suitable, though [Visual Studio Code](https://code.visualstudio.com) is recommended due to the excellent support for file formats through available extensions.

To start the platform development, open the repository folder in the IDE.

Autosave might be helpful. To enable it in Visual Studio Code, select *File → Auto Save*.

If the IDE can not save changes, it might be a file permissions problem. Refer to the [issues section](./issues.md#versioning) for the solution.

## Changes Propagation

To apply changes in the `themes` folder without rebooting the platform, run `bmv_devtheme` after launching the development environment.

To see the changes, use `bmv_render` or `bmv_save`, and the alias mentioned above ensures that the `env` folder will be automatically regenerated.

If files or folders have been added or renamed, the development environment must be rebuilt even if `dev_theme` is running. It applies to new files but not necessarily included files, like imported SCSS files or overrides of existing page templates.

To see these changes in the production environment, it is always necessary to build it using `bmv_build`.

## Customisation Options

The BioMedVis can be modified by using plugins, themes and page templates.

In case more information is necessary, refer to the relevant topics for the Open edX platform. It is recommended to search for Tutor distribution information, as, for instance, plugins are a Tutor-specific function.

### Plugins

Tutor plugins are either YAML files or Python packages. This manual focuses on YAML files, as they are suitable for the BioMedVis needs. Refer to the source for more information - the [plugins documentation](https://docs.tutor.overhang.io/plugins/v0/index.html).

#### Plugin Categories

- The plugins can modify the platform in multiple ways, depending on the chosen category. The categories are named *config*, *commands*, *patches*, *templates* and *hooks*. Plugins using *patches*, *templates* and *hooks* categories alter the platform's page templates. The main practical difference between Python packages and YAML files is that the YAML files can only affect the *config* and *patches* categories.

  - *Config* works with the platform's configuration settings.
  - *Commands* attribute modifies commands controlling the platform through the CLI.
  - *Patches* affects already rendered page templates by applying modifications on top of them.
  - *Templates* attribute points to the *hooks* directory.
  - *Hooks* directory contains actions triggered when the defined events occur.

#### Plugin Controls

- Before working with plugins, it is necessary to check that the plugin source is in the desired folder - alias `bmv_srcplugins` prints the plugin source path. The source should point to the `plugins` folder in the BioMedVis. To change the source location, refer to the [installation section](./installation.md).

- To list all platform plugins, or toggle their state, refer to the [commands section](./commands.md#plugin-controls).

#### Platform Plugins

- The BioMedVis platform contains two plugins located in the `plugins` folder.

  - `bmv-other-settings`:
    - Enables the Open edX functionality named *other course settings*.
    - It allows adding custom course parameters so that the lecturer can display additional information about the course.
    - Using this plugin, course *tags* and *difficulties* work.

  - `bmv-custom-urls`:
    - Enables custom URLs for new platform pages.
    - It informs the platform that it contains additional static pages.
    - Using this plugin, *my achievements* and *course map* work.

### Themes

The BioMedVis platform uses the Indigo theme. Refer to the [official GitHub](https://github.com/overhangio/tutor-indigo) for more information. This source also includes installation instructions, including enabling the theme.

The platform repository is based on the `lilac.3` tag in the Open edX repository, so the theme is installed using a Python package, not a Tutor package.

#### Theme Structure

- The repository also includes theme modifications.
- The structure is described in the [repository section](./repository.md#theme-folder).

#### Customise Theme

- The modifications are applied to the ` theme/lms/static` folder to customise the theme.
  - The folder named `images` contains the website's favicon, which is visible in a web browser, as well as the website's logo, which is visible on the website. These files can be easily replaced, as stated in the [Indigo documentation](https://github.com/overhangio/indigo).
  - The `sass` folder contains SCSS, which forms the platform's look. These files define font preferences, custom variables and extra styling. The styling resides in the `_extras.scss` file, which I modified to import a file named `_custom-look.scss`. In this file, any custom CSS can be inserted.

### Page Templates

By using page templates, the existing pages can be modified, and new pages can be added.

#### Modify Templates

- Changes are made inside of `theme/lms/templates` folder.
- As stated on the [official GitHub](https://github.com/overhangio/tutor-indigo), the added templates override the original files. 
- Thus, it is crucial to place the new file into the BioMedVis repository folder that matches the corresponding folder in the Open edX repository. Upon using `bmv_render`, the old template is overridden with the new one.
- The templates are modified based on the `lilac.3` tag in the Open edX repository. To visit it, follow the [GitHub URL](https://github.com/openedx/edx-platform/tree/open-release/lilac.3/lms/templates).
- The repository contains all the platform files and folders.
- It should be noted that files in the `lilac.3` tag in the Open edX repository work the most consistent with the BioMedVis setup, as the Tutor version is based on this tag.
- After finding the desired template, it is necessary to copy its contents, and the already mentioned approach can be used to apply changes.

#### Platform Templates

- New pages can be added to the platform using the `bmv-custom-urls` plugin.
- To apply changes, use `bmv_save` and `bmv_render` and rebuild the platform image using `bmv_allbuild`. 
- It is recommended to derive its content from a similar existing template and build on it.
- The page template can be edited using any of the before-mentioned techniques.

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Versioning](../docs/versioning.md)
