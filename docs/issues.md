◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# ⚠️ Common Issues

During the platform development, various problems might occur.

## Docker 

### Error 1 or 137

If Docker exits with an error, the Docker Engine must be restarted. It usually happens on WSL installations. To restart the Docker, follow the steps below.

1. Use `bmv_stop` to stop the platform.
2. If the machine runs on Windows, locate the Docker icon in the Windows taskbar and select *Restart Docker…*. On Linux, proceed with the equivalent procedure.
3. Wait until all Docker containers are reloaded.
4. If the issue is not resolved at this point, reboot the WSL or the Linux machine. Following, reboot the Docker for Windows again.

## Versioning

### Commands Fail

If saving the changes or `git add` or `git commit` commands fail, the files and folders might have insufficient access rights. To fix it, use `sudo chmod -R 777 /bmv` to assign sufficient access rights.

### SSL Certificate Error

If you encounter an SSL certificate error in the process, the best temporary solution is to retry the command. It usually happens on the WSL setups due to system time being behind, and it can be fixed by syncing the time using `hwclock -s`.

<br>

---

◀️ [Go Back](../README.md#manuals)
