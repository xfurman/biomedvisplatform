◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 💡 Platform Control

To start the production platform, use `bmv` command.

- LMS is available at [local.overhang.io](http://local.overhang.io).
- Studio is available at [studio.local.overhang.io](http://studio.local.overhang.io).

In order to stop the platform, use `bmv_stop` command.

Visit the platform page and sign in or register to start using the platform. 

The platform's user can be a student, a lecturer, or an administrator.

## Course Management

Courses are managed in the Studio.

Every BioMedVis course can also have 🟢 **Bio**, 🔵 **Med** or 🟠 **Vis** tag.

Additionally, course difficulty can be specified as a **1️⃣ Novice**, **2️⃣ Intermediate** or **3️⃣ Master**.

The platform's *Other Course Settings* must be enabled to use these settings.

### Add Custom Settings

In order to use the settings mentioned above, or to include other settings, follow the steps below.

1. Open the Studio and select the course you wish to modify.
2. In the header, click on *Settings* and select *Advanced Settings* from the dropdown.
3. On the opened page, search for the *Other Course Settings* field with the input box right next to it.
4. Add a valid dictionary of values in JSON format here, where the values are as follows:
    - `bmv-bio`, `bmv-med` and `bmv-vis` are booleans representing the 🟢 **Bio**, 🔵 **Med** and 🟠 **Vis** tags.
    - `difficulty` is an integer representing the **1️⃣ Novice**, **2️⃣ Intermediate** or **3️⃣ Master** level.
5. When finished, click on *Save Changes* in the bottom bar informing about setting changes.
6. Enjoy the added settings.

#### Example of Other Course Settings

```json
{
    "bmv-bio": true,
    "bmv-med": false,
    "bmv-vis": true,
    "difficulty": 2
}
```

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Repository Structure](../docs/repository.md)
