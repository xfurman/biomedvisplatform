◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 🌳 Versioning

The current version is in the `master` branch, while backups of already-merged branches are available through git tags. These tags also contain backups of branches that were discontinued without merging.

If you encounter git command errors or an SSL certificate error in the process, refer to the [issues section](./issues.md#versioning) for the solution.

Please note that using `sudo` might be necessary while performing certain git operations. Optionally, use `sudo chmod -R 777 /bmv` to assign sufficient access rights.

## Commit Procedure

It is recommended to stop the platform using `bmv_stop` before working with git branches to avoid data inconsistency.

It is also possible to use aliases for git, located in the [](./bmv-commands.txt) file. Use them only if you know what they are doing to avoid data loss.

1. Navigate to `/bmv/biomedvisplatform` folder.
2. In case the database data are backed up using git, proceed with `git add data` followed by `git commit -m <your-message>`.
3. Continue with `git add .` followed by `git commit -m <your-message>`.
4. Use `git push`.

## Switching Branches

It is recommended to stop the platform using `bmv_stop` before switching branches, as it may lead to unexpected behaviour.

After switching the branches, Docker might stop working correctly. To fix it, restart the Docker Engine.

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Hosting Information](../docs/hosting.md)
