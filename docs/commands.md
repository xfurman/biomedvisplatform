◀️ [Go Back](../README.md#manuals)

<sup>**BioMedVis Platform**</sup>

# 🌟 Commands

This manual contains the description of the BioMedVis platform commands, which are created using aliases.

View the [`docs/bmv-commands.txt`](../docs/bmv-commands.txt) file to review the aliases' code. To view the complete list of Open edX commands, refer to the source - the [CLI reference](https://docs.tutor.overhang.io/reference/cli/index.html).

## Setup

To set up the command aliases for the platform:

1. Copy the contents of [`docs/bmv-commands.txt`](../docs/bmv-commands.txt) file.
2. To add the commands, edit the shell script using `nano ~/.bashrc`.
3. Go to the end of the `~/.bashrc` file and add the copied contents there.
4. Save changes and reopen bash to use the commands.

## List of Commands

### Platform Services

- `bmv` starts the BioMedVis platform at [local.overhang.io](http://local.overhang.io), including Studio at [studio.local.overhang.io](http://studio.local.overhang.io). 
- `bmv_dev` starts the BioMedVis platform in the development mode at [local.overhang.io:8000](http://local.overhang.io:8000), without Studio.
- `bmv_devstudio` acts as `bmv_dev`, but with Studio available at [studio.local.overhang.io:8001](http://studio.local.overhang.io:8001).
- `bmv_devtheme` runs the previously mentioned `watchtheme` command alias that watches theme changes. To see the changes, `bmv_dev` must be running, and either `bmv_render` or `bmv_save` should be called.
- `bmv_restart` quickly restarts all the services. It is useful for reloading settings without performing the full shutdown.
- `bmv_stop` shuts down the platform, both production and development.

### Platform Controls

- `bmv_src` prints the current path to the source folder.
- `bmv_quickstart` runs the platform for the first time while initialising its services. This command alias also needs to be run after upgrading Tutor to a newer version, as it updates the repository and applies database migrations. Before this command alias, run `bmv_save` and `bmv_allbuild`.
- `bmv_init` initialises the platform after the Docker container changes and fixes services that might have been unavailable, such as Tutor Forum.

### Plugin Controls

- `bmv_srcplugins` prints the current path to the plugin folder.
- `bmv_plugins` lists all included platform plugins.
- `bmv_enable id` enables the plugin specified by an `id`, which is the same as the plugin name. To apply changes, run `bmv_save`.
- `bmv_disable id` disables the plugin specified by an `id`, which is the same as the plugin name. To apply changes, run `bmv_save`.

### Source Changes

- `bmv_save` saves configuration. It is useful when the core changes have been made.
- `bmv_render` regenerates theme and template files.
- `bmv_build` builds the platform image.
- `bmv_devbuild` builds the platform image for development mode. To have new changes applied, the `bmv_build` command alias must be run prior to this one.
- `bmv_allbuild` builds all platform images, including the development image.

<br>

---

◀️ [Go Back](../README.md#manuals)
| Next manual: [Platform Control](../docs/platform.md)
