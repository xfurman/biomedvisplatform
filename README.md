# BioMedVis Platform

BioMedVis platform is a learning platform that connects biological and medical visualisations. The courses are categorised using 🟢 **Bio**, 🔵 **Med** and 🟠 **Vis** tags, and the courses have varying difficulty levels specified by 1️⃣, 2️⃣ and 3️⃣ labels.

**[MANUALS](#manuals)** are referenced below the introductory information.

<br>

## Integral Parts

As is usual with education platforms, the BioMedVis platform consists of two main parts - a learning management system and a content management system. For more information, refer to the source - the [edX website](https://studio.edx.org).

>### Learning Management System (LMS)
> 
> - The published courses reside there, and the users can interact with them.
> - Student's view of the platform.

> ### Content Management System (CMS)
>
> - So-called Studio.
> - The integrated environment for creating and managing courses.
> - Lecturer's/Administrator's view of the platform.

<br>

## Used Technologies

The platform uses multiple technologies included in the **[MANUALS](#manuals)**.

> ### Open edX Platform
> 
> - It provides the LMS and CMS functionality.
> - For more information, refer to the source - the [official website](https://openedx.org).
> - Links:
>    - [Documentation for edX](https://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/index.html) - the primary source.
>    - [Official Open edX GitHub](https://github.com/openedx/edx-platform/tree/open-release/lilac.3/lms/templates) - `lilac.3` tag data are used for the BioMedVis platform.

> ### Tutor Distribution
> 
> - It simplifies works with the Open edX platform by using Docker containers for application processes.
> - For more information, refer to the source - the [official website](https://overhang.io/tutor).
> - Links:
>    - [Documentation for Tutor](https://docs.tutor.overhang.io) - the primary source.
>    - [Official Tutor GitHub](https://github.com/overhangio/tutor) - includes known issues.
>    - [Tutor discussion forum](https://discuss.overhang.io) - contains debugging information.

> ### Indigo Theme
> 
> - It is the chosen theme for the BioMedVis platform, developed by Overhang, the company behind Tutor distribution.
> - Links:
>    - [Official Indigo GitHub](https://github.com/overhangio/indigo) - includes setup instructions.

<br>

## Manuals

These manuals cover the BioMedVis platform functionalities. It is unnecessary to read them in chronological order, as the related manuals are referenced in the proper locations.

The title of every manual is clickable!

### ⬇️ [Installation Instructions](docs/installation.md)

- Instructions for setting up the BioMedVis platform.
- Suitable for Linux or Windows 11/10 with WSL installed.

### 🌟 [Commands](docs/commands.md)

- Platform commands that simplify the operation and development.
- Written as aliases.

### 💡 [Platform Control](docs/platform.md)

- Basics of operating the production version.
- Includes additional course tags and difficulties information.

### 🧩 [Repository Structure](docs/repository.md)

- Structure of the BioMedVis repository, including Tutor and theme information.
- Includes detailed folder descriptions.

### 🔬 [Further Development](docs/development.md)

- Information about operating the development version and modifying the platform.
- Includes plugins, themes and page templates.

### 🌳 [Versioning](docs/versioning.md)

- GitLab repository describing platform-specifics.
- Includes information about the current state of the repository.

### 📢 [Hosting Information](docs/hosting.md)

- Information about the production version of the platform.
- Details the Stratus.FI hosting.

### ⚠️ [Common Issues](issues.md)

- Solutions for common issues.
- Mentions Docker and versioning.

<br>

---

<sup>GitLab manuals were created as a part of the *BioMedVis Learning Platform* thesis.</sup>
